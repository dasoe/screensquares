#include "ofApp.h"

//--------------------------------------------------------------

void ofApp::setup() {
    ofSetFrameRate(10);
   
    settings.addInt("columns", 16);  
    settings.addInt("wait", 5);  
    settings.addInt("border", 5);  
    settings.init("settings.xml",true);  
    
    palette.push_back(ofColor(244, 229, 0));
    palette.push_back(ofColor(253, 198, 11));
    palette.push_back(ofColor(241, 142, 28));
    palette.push_back(ofColor(234, 98, 31));
    palette.push_back(ofColor(227, 35, 34));
    palette.push_back(ofColor(196, 3, 125));
    palette.push_back(ofColor(109, 57, 139));
    palette.push_back(ofColor(68, 78, 139));
    palette.push_back(ofColor(42, 113, 176));
    palette.push_back(ofColor(6, 150, 187));
    palette.push_back(ofColor(0, 142, 91));
    palette.push_back(ofColor(140, 187, 38));

    ofSetBackgroundAuto(false);
    // related to a skipping first frames bug in ofSetBackgroundAuto == false;
    readyToDraw = false; 
    firstRun = true;
    
    calcRowsAndCols();
    
    timer = ofGetElapsedTimeMillis();
    
    fbo.allocate( ofGetWidth(), ofGetHeight() , GL_RGB);
    lastFbo.allocate( ofGetWidth(), ofGetHeight() , GL_RGB);
    
    helpFbo.allocate(500,120, GL_RGBA);
    
    mode = 0;
    
    
    
    //ofDisableAlphaBlending();
    ofDisableAntiAliasing();
    
    //    for (int i = 0; i < 9; i++) {
    //        for (int j = 0; j < 16; j++) {
    //
    //        }
    //    };

}

//--------------------------------------------------------------

void ofApp::update() {
    settings.update();
    actualTime = ofGetElapsedTimeMillis();
    
    // firstRun stuff related to a skipping first frames bug in ofSetBackgroundAuto == false;    
    if ( 
        (actualTime - timer) >  settings.getIntValue("wait") ||
        firstRun && (actualTime - timer) >  500 
            
        ) {
        readyToDraw = true;
        firstRun = false;        
        timer = ofGetElapsedTimeMillis();
    }

}

//--------------------------------------------------------------

void ofApp::draw() {
    if (readyToDraw) {
        fbo.begin();
        ofClear(0);
        lastFbo.draw(0,0);
        if (!initialized) {
            ofLogNotice("##### INIT #######");
            ofSetBackgroundColor(0, 0, 0);
            ofClear(0);

            for (int i = 0; i < rows; i++) {
                for (int j = 0; j < columns; j++) {
                    int index = (int) ofRandom(0, palette.size());
                    ofSetColor(palette[index]);
                    ofDrawRectangle(j*width +settings.getIntValue("border"), i*height +settings.getIntValue("border"), width-(2*settings.getIntValue("border")), height-(2*settings.getIntValue("border")));
                }
                if (i == rows-1) {
                    initialized = true;
                }
            };
        } else {
            lastFbo.draw(0,0);
            ofLogNotice("## DRAW ONE ##");
            int col = (int) ofRandom(0, 16);
            int row = (int) ofRandom(0, 9);
            int index = (int) ofRandom(0, palette.size());
            ofSetColor(palette[index]);

            ofDrawRectangle(col*width + settings.getIntValue("border"), row*height + settings.getIntValue("border"), width-(2*settings.getIntValue("border")), height-(2*settings.getIntValue("border")));
            readyToDraw = false;
        }
        fbo.end();
    }

    lastFbo.begin();
        ofClear(0);     
        fbo.draw(0,0);
    lastFbo.end();
    
    fbo.draw(0,0);
    
    if (mode) {
        helpFbo.begin();
        ofClear(0);
        ofDrawBitmapString("spalten (p/l): " + ofToString(settings.getIntValue("columns")), 5,15 );
        ofDrawBitmapString("pause (o/k): " + ofToString(settings.getIntValue("wait")), 5,30 );
        ofDrawBitmapString("border (i/j): " + ofToString(settings.getIntValue("border")), 5,45 );
        helpFbo.end();
        ofSetColor(0);
        helpFbo.draw(1,1);
        ofSetColor(255);
        helpFbo.draw(0,0);
    }

}

//--------------------------------------------------------------

void ofApp::keyPressed(int key) {
    switch (key) {
        case 's':
            mode = !mode;
        break;
        case 'p':
            if (mode) {
                columns += 1;
                settings.setIntValue("columns", columns, true);
                calcRowsAndCols();                
            }
        break;
        case 'l':
            if (mode) {
                columns -= 1;
                settings.setIntValue("columns", columns, true);
                calcRowsAndCols();                
           }
        break;
        case 'o':
            if (mode) {
                settings.setIntValue("wait", settings.getIntValue("wait")+ 250, true);
                calcRowsAndCols();                
           }
        break;
        case 'k':
            if (mode) {
                settings.setIntValue("wait", settings.getIntValue("wait")-250, true);
                calcRowsAndCols();                
           }
        break;
        case 'i':
            if (mode) {              
                settings.setIntValue("border", settings.getIntValue("border")+ 1, true);
                calcRowsAndCols();                
           }
        break;
        case 'j':
            if (mode) {
                settings.setIntValue("border", (settings.getIntValue("border")-1), true);
                calcRowsAndCols();                
           }
            
        break;
        
    }

}

//--------------------------------------------------------------

void ofApp::keyReleased(int key) {

}


//--------------------------------------------------------------

void ofApp::calcRowsAndCols() {
    settings.update();
    
    width = ofGetWidth() / settings.getIntValue("columns");
    ofLogVerbose( "tiles horizontal for calculation: " + ofToString(settings.getIntValue("columns")) );
    ofLogVerbose( "width: " + ofToString(width) );
    float tilesV = ofGetHeight() / width;
    ofLogVerbose( "tiles vertical for calculation: " + ofToString(tilesV) );
    
    float heightV = ofGetHeight() / round(tilesV);
    height = (int) heightV;
    ofLogVerbose( "height: " + ofToString(height) );
    
    columns = settings.getIntValue("columns");
    rows = round(tilesV);
    ofLogVerbose( "tiles horizontal: " + ofToString( columns ) );
    ofLogVerbose( "tiles vertical: " + ofToString( rows ) );
    
    initialized = false;
    // change timer so that ist shall be executed immediately;
    timer = actualTime - settings.getIntValue("wait") + 200;
    
}