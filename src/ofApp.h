#pragma once

#include "ofMain.h"
#include "ofxXmlBasedProjectSettings.h"

class ofApp : public ofBaseApp{
	public:
		void setup();
		void update();
		void draw();

		void keyPressed(int key);
		void keyReleased(int key);
		void calcRowsAndCols();
                
                vector <ofColor> palette;
                
                float width, height;
                bool initialized, readyToDraw, firstRun;
                
                long actualTime, timer;
                int mode, columns, rows;
                
                ofFbo fbo, lastFbo, helpFbo;
                
                ofxXmlBasedProjectSettings settings;
                
};
